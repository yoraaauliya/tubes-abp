<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BookTrip | Destination</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    @include('partial.head')

    <style>
        body {
            background-image: url('{{ asset('template/images/liveaboard-indonesia-lombok-volcano-xxl.jpg')}}');
        }
        
        .background {
            width: 430px;
            height: 520px;
            position: absolute;
            transform: translate(-50%, -50%);
            left: 50%;
            top: 50%;
        }
    </style>

  </head>
  <body>
    <div class="background">
        <div class="shape"></div>
        <div class="shape"></div>
    </div>
	<div class="container ftco-animate" style="margin-top: 5px">
        <div class="row">
          <div class="col-lg-6 mx-auto">
            <div class="card ">
              <div class="card-header">
                <h3>Payment</h3>
                <div class="bg-white shadow-sm pt-4 pl-2 pr-2 pb-2">
                  <!-- Credit card form tabs -->
                  <ul role="tablist" class="nav bg-light nav-pills rounded nav-fill mb-3">
                    <li class="nav-item"> <a data-toggle="pill" class="nav-link active"> <i
                          class="fas fa-mobile-alt mr-2"></i> Net Banking </a> </li>
                  </ul>
                </div> <!-- End -->
                <!-- bank transfer info -->
                <div class="tab-pane pt-3">
                  <form method="POST" action="/order">
                    @csrf
                    
                    <div class="form-group "> <label for="Select Your Bank">
                        <h6>Select your Bank</h6>
                    </label> <select class="form-control" name="bank" required>
                        <option value="" selected disabled>--Please select your Bank--</option>
                        <option value="BCA">BCA</option>
                        <option value="Mandiri">Mandiri</option>
                        <option value="BNI">BNI</option>
                        <option value="BRI">BRI</option>
                        <option value="BJB">BJB</option>
                    </select> </div>
                    <div class="form-group ">
                        <label for="Select Your Bank">
                            <h6>Email</h6>
                        </label>
                        <input type="email" class="text form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group ">
                        <label for="Select Your Bank">
                            <h6>No Handphone</h6>
                        </label>
                        <input type="text" class="text form-control" id="noHp" name="noHp" required>
                    </div>
                    <div class="form-group " style="visibility: hidden; display: inline;">
                      <input class="text" name="user_id" value="{{ Auth::user()->id }}">
                    </div>
                    <div class="form-group " style="visibility: hidden; display: inline;">
                      <input class="text" name="dest_id" value="{{$destinasi->id}}">
                    </div>
                    <div class="form-group">
                      <p><button class="btn btn-primary btn-lg" style="border-radius: 8px;">Reserve</button></p>
                    </div>
                    <p class="text-muted">Note: After clicking on the button, you will be directed to a secure gateway for
                      payment. After completing the payment process, you will be redirected back to the website to view
                      details of your order. </p>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </body>
  @include('partial.script')
</html>