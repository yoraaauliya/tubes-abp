<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('partial.head')
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partial.navbar')
    </nav>

    <section class="ftco-section">
    	<div class="container">
            <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">User ID</th>
                    <th scope="col">Destination ID</th>
                    <th scope="col">Bank</th>
                    <th scope="col">No HP</th>
                    <th scope="col">Email</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($order as $key=>$item)
                        <tr>
                            <td>{{$item->user_id}}</td>
                            <td>{{$item->dest_id}}</td>
                            <td>{{$item->bank}}</td>
                            <td>{{$item->noHp}}</td>
                            <td>{{$item->email}}</td>                                           
                        </tr>                         
                    @empty
                        <tr>
                            <td>Data Kategori Kosong</td>
                        </tr>
                    @endforelse  
                </tbody>
            
              </table>
      </div>
    </section>


</body>

@include('partial.script')
</html>