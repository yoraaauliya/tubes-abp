<!DOCTYPE html>
<html lang="en">
	<head>
		<title>BookTrip</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		@include('partial.head')
		
	</head>
  <body>
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    @include('partial.navbar')
        
	  </nav>
    <!-- END nav -->
    
    <div class="hero-wrap js-fullheight" style="background-image: url('{{ asset('template/images/bg.jpg')}}'); data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
          <div class="col-md-9 text text-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
            <p class="caps" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Liburan ke berbagai sudut pulau Lombok, hanya dengan satu sentuhan</p>
            <h1 data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Membuat Liburan Menjadi Mudah</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section services-section bg-light">
		@include('partial.about')
    </section>

	<section class="ftco-section">
		@include('partial.bestDestination')
    </section>

    <section class="ftco-section testimony-section bg-bottom" style="background-image: url('{{ asset('template/images/bg_3.jpg')}}');">
      @include('partial.feedback')
    </section>

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  @include('partial.script')
  </body>
</html>