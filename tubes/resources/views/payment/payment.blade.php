
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BookTrip | Destination</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    @include('partial.head')
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partial.navbar')
    </nav>

      <div class="container">
        <div class="row" style="justify-content: center;">
            <div class="image">
                <img src="{{asset('template/images/icon/Group 1.png')}}" alt="" style="width: 680px;">
            </div>
        </div>
        <div class="row" style="justify-content: center;">
            <h3 style="color: black;">Your<span style="color: #f9ab30;"> Payment</span> is Successful</h3>
        </div>
      </div>

  @include('partial.script')
    
  </body>
</html>