<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BookTrip | Destination</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    @include('partial.head')

  </head>
  <body>
	<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partial.navbar')
    </nav>
    <!-- END nav -->

    <section class="ftco-section">
    	<div class="container">
        <div class="row" style="justify-content: center;">
        	<div class="col-md-auto ftco-animate" style="width: 480px; height: 490px;">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" style="border-radius: 8px;">
                      <div class="carousel-item active">
                        <img class="d-block w-100" src="{{ asset('gambar/img_1/' . $destinasi->foto_1) }}" alt="First slide" style="width: 480px; height: 580px;">
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="{{ asset('gambar/img_2/' . $destinasi->foto_2) }}" alt="Second slide" style="width: 480px; height: 580px;">
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="{{ asset('gambar/img_3/' . $destinasi->foto_hotel) }}" alt="Third slide" style="width: 480px; height: 580px;">
                      </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                </div>
        	</div>
            <div class="col-md-auto ftco-animate">
                <div class="card ftco-animate" style="width: 480px; border-radius: 8px; height: 580px;">
                    <div class="card-body">
                        <h5 class="card-title">Informasi Tour</h5>
                        <div class="row" style="margin-left: 2px;">
                            <div class="col">
                                <p style="font-weight: 600;">{{$destinasi->destinasi}}</p>
                                <p><img src="{{asset('template/images/icon/icons8-plane-64.png')}}" alt="" style="width: 24px;"> {{$destinasi->keberangkatan}}</p>
                                <p><img src="{{asset('template/images/icon/icons8-ticket-confirmed-80.png')}}" alt="" style="width: 24px;"> Tiket Masuk Pariwisata</p>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2px;">
                                <div class="col">
                                    <p><img src="{{asset('template/images/icon/icons8-hotel-50.png')}}" alt="" style="width: 24px;"> {{$destinasi->durasi_tour}} Days</p>
                                    <p><img src="{{asset('template/images/icon/icons8-tour-guide-60.png')}}" alt="" style="width: 24px;"> {{$destinasi->durasi_tour_guide}} Days Tour</p>
                                </div>
                                <div class="col">
                                    <p><img src="{{asset('template/images/icon/icons8-user-account-90.png')}}" alt="" style="width: 24px;"> 4 Guest</p>
                                </div>
                        </div>
                        <h5 class="card-title">Informasi Hotel</h5>
                        <div class="row" style="margin-left: 2px;">
                            <div class="col"><p style="font-weight: 600;" >{{$destinasi->lokasi_hotel}}</p></div> 
                        </div>
                        <div class="row" style="margin-left: 4px;">
                            <div class="col-md-auto">
                                <p><img src="{{asset('template/images/icon/icons8-bed-50.png')}}" alt="" style="width: 24px;"> {{$destinasi->bed}}</p>
                                <p><img src="{{asset('template/images/icon/icons8-bathtub-100.png')}}" alt="" style="width: 24px;"> {{$destinasi->bathroom}}</p>
                                <p><img src="{{asset('template/images/icon/icons8-wi-fi-50.png')}}" alt="" style="width: 24px;"> Wi-Fi</p>
                            </div>
                            <div class="col-md-auto">
                                <p><img src="{{asset('template/images/icon/icons8-air-conditioner-100.png')}}" alt="" style="width: 24px;"> Ac</p>
                                <p><img src="{{asset('template/images/icon/icons8-water-heater-64.png')}}" alt="" style="width: 24px;"> Water Heater</p>
                            </div>
                        </div>
                      <div class="row" style="padding: 24px;">
                          <a href="/id/{{$destinasi->id}}" class="btn btn-primary w-100" style="border-radius: 8px; margin-top: -12px;">Rp.{{$destinasi->harga}}</a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ftco-animate" style="border-top: 1px solid #DBE4FF; margin-top: 24px; display: flex; flex-direction: column">
            <div class="col">
                <h3 style="margin-top: 8px;">{{$destinasi->destinasi}}</h3>
            </div>
            <div class="col">
                <p>{{$destinasi->desc}}</p>
            </div>
        </div>
    	</div>
        
    </section>


  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


</body>
    @include('partial.script')
</html>