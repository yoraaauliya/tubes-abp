<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('partial.head')
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partial.navbar')
    </nav>
    <section class="ftco-section">
		@include('partial.bestDestination')
    </section>


    <section class="ftco-section ftco-no-pt">
    	<div class="container">
    		<div class="row justify-content-center pb-4">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Tour Destination</h2>
          </div>
        </div>
        <div class="row">
            @foreach ($destinasi as $item)
        	<div class="col-md-4 ftco-animate">
        		<div class="project-wrap">
                    <a href="/destination/{{$item->id}}" class="img" style="background-image: url({{ asset('gambar/img_1/' . $item->foto_1) }}) ;"></a>
        			<div class="text p-4">
        				<span class="price">Rp.{{ $item->harga }}</span>
        				<span class="days">{{ $item->durasi_tour }} Tour Days</span>
        				<h3><a href="#">{{ $item->destinasi }}</a></h3>
        				<p class="location"><span class="ion-ios-map"></span> {{ $item->lokasi_hotel }}</p>
        				<ul>
        					<li><span class="flaticon-shower"></span>{{ $item->bathroom }}</li>
        					<li><span class="flaticon-king-size"></span>{{ $item->bed }}</li>
        				</ul>
        			</div>
        		</div>
        	</div>
            @endforeach
        </div>
    </section>
















    {{-- <section class="ftco-section ftco-no-pt">
    	<div class="container">
    		<div class="row justify-content-center pb-4">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Tour Destination</h2>
          </div>
        </div>
        </div>
        <div class="row">
            <div class="card-deck">
                <div class="row px-3 py-3" >
                    @forelse ($destinasi as $item)
                        <div class="col-4" >
                            <div class="card my-3" style="width: 18rem; margin: 5px">
                                <img class="card-img-top" src="{{ asset('gambar/' . $item->foto_1) }}" alt="Card image cap"
                                    style="width: 288px; height: 205px; object-fit: cover;">
                                <div class="card-body">
                                    <div class="d-flex row " style="justify-content: center; margin-top:0px">
                                        <p class="card-title text-primary">Rp. {{ $item->harga }}</p>
                                    </div>
                                    <div class="d-flex row justify-content-between">
                                        <p class="card-title">{{ $item->destinasi }}</p>
                                        
                                    </div>
                                    <div class="d-flex row justify-content-between align-items-center mb-2">
                                        <p class="card-text">Available: {{ $item->kamar }}</p>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    @empty
                        <h3>Tidak Ada Produk Terkini</h3>
                    @endforelse
                </div>
            </div>
        </div>  
    </section> --}}

</body>

@include('partial.script')
</html>

