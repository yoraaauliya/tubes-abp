<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('partial.head')
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partial.navbar')
    </nav>

    <section class="ftco-section">
    	<div class="container" style="margin-top: 24px; margin-left: 12px">
            <div class="row"  style="justify-content: center">
                <div class="card ftco-animate" style="width: 320px;">
                    <div class="card-body">
                        <div class="row"  style="justify-content: center">
                            <h4 class="card-title">Informasi User</h4>
                            <form action="/User" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label >Full Name :</label>
                                    <input type="text" class="form-control" name="name">
                                </div>              
                                <div class="form-group">
                                    <label >Email :</label>
                                    <input type="email" class="form-control" name="email">
                                </div>               
                                <div class="form-group">
                                    <label >Password :</label>
                                    <input type="password" class="form-control" name="password">
                                </div>       
                                <div class="row" style="padding: 12px">
                                    <button type="submit" class="btn btn-primary w-100" style="border-radius: 9px">Submit</button>
                                </div>           
                            </form>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </section>


</body>

@include('partial.script')
</html>

