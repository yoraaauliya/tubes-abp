<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('partial.head')
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partial.navbar')
    </nav>

    <section class="ftco-section">
    	<div class="container" style="margin-left: -12px">
        <div class="row" style="justify-content: center;">
            <div class="card ftco-animate" style="width: 480px; margin-top: 12px;">
                <h4 class="card-title" style="margin-left: 116px;margin-top: 24px">Informasi Tour</h4>
                <div class="row" style="justify-content: center; padding: 24px">
                        <form action="/destination/{{$destinasi->id}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="form-group">
                                <label >Destinasi :</label>
                                <input type="text" class="form-control" value="{{$destinasi->destinasi}}" name="destinasi" required>
                            </div>       
                            @error('destinasi')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >Keberangkatan :</label>
                                <input type="text" class="form-control" name="keberangkatan" value="{{$destinasi->keberangkatan}}" required>
                            </div>       
                            @error('keberangkatan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >Durasi Tour :</label>
                                <input type="text" class="form-control" name="durasi_tour" value="{{$destinasi->durasi_tour}}" required>
                            </div>       
                            @error('durasi_tour')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            
                            <div class="form-group">
                                <label >Durasi Tour-Guide :</label>
                                <input type="text" class="form-control" name="durasi_tour_guide" value="{{$destinasi->durasi_tour_guide}}" required>
                            </div>       
                            @error('durasi_tour_guide')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            
                            <div class="form-group">
                                <label >Nama Hotel :</label>
                                <input type="text" class="form-control" name="lokasi_hotel" value="{{$destinasi->lokasi_hotel}}" required>
                            </div>       
                            @error('lokasi_hotel')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >Bed :</label>
                                <input type="number" class="form-control" name="bed" value="{{$destinasi->bed}}" required>
                            </div>        
                            @error('bed')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >Bathroom :</label>
                                <input type="number" class="form-control" name="bathroom" value="{{$destinasi->bathroom}}" required>
                            </div>       
                            @error('bathroom')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >Deskripsi Tour :</label>
                                <textarea class="form-control" name="desc"  required>{{$destinasi->desc}}</textarea>
                            </div>       
                            @error('desc')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >Harga :</label>
                                <input type="number" class="form-control" name="harga" value="{{$destinasi->harga}}" required>
                            </div>       
                            @error('harga')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            
                            <button type="submit" class="btn btn-primary w-100" style="border-radius: 9px">Submit</button>
                        </form>
                    </div>
            </div>
        </div>
      </div>
    </section>


</body>

@include('partial.script')
</html>

