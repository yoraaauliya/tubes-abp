<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('partial.head')
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partial.navbar')
    </nav>

    <section class="ftco-section">
    	<div class="container">
            <a href="/destination/create" class="btn btn-primary btn-sm my-2" style="border-radius: 9px" >Tambah Destinasi</a>
            <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Destinasi</th>
                    <th scope="col">Keberangkatan</th>
                    <th scope="col">Durasi Tour</th>
                    <th scope="col">Hotel</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($destinasi as $key=>$item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->destinasi}}</td>
                            <td>{{$item->keberangkatan}}</td>
                            <td>{{$item->durasi_tour}}</td>
                            <td>{{$item->lokasi_hotel}}</td>
                            <td>{{$item->harga}}</td>
                            <td>
                                <form action="/destination/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <a href="/destination/{{$item->id}}/edit" class="btn btn-sm btn-warning" style="border-radius: 9px">Edit</a>
                                    <input type="submit" class="btn btn-sm btn-danger" value="Delete" style="border-radius: 9px">
                                </form>
                            </td>                                                   
                        </tr>                         
                    @empty
                        <tr>
                            <td>Data Kategori Kosong</td>
                        </tr>
                    @endforelse  
                </tbody>
            
              </table>
      </div>
    </section>


</body>

@include('partial.script')
</html>

