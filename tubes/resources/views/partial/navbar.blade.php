<div class="container">
  <a class="navbar-brand" href="/">BOOKTRIP<span>Lombok Trip</span></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav"
    aria-expanded="false" aria-label="Toggle navigation">
    <span class="oi oi-menu"></span> Menu
  </button>
  
  <div class="collapse navbar-collapse" id="ftco-nav">

    <ul class="navbar-nav ml-auto">
      @guest
      <li class="nav-item cta"><a href="/login" class="btn btn-primary nav-link">Login</a></li>
      @endguest
      
      @auth
      @if (Auth::user()->role == 'admin')
      <div class="dropdown">
        <a class="profile dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown"
          aria-expanded="false" style="margin-top: 23px; color:white">Manage</a>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
          <li><a class="dropdown-item" href="/crudDestination">Manage Destinasi</a></li>
          <li><a class="dropdown-item" href="/User">Manage User</a></li>
        </ul>
      </div>
      @endif

      <li class="nav-item"><a href="/" class="nav-link">Home</a></li>
      <li class="nav-item"><a href="/about" class="nav-link">About</a></li>
      <li class="nav-item"><a href="/destination" class="nav-link">Destination</a></li>
      <div class="dropdown">
        <a class="profile dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown"
          aria-expanded="false"><img src="{{asset('template/images/icon/icons8-account-48.png')}}" alt=""
            style="width: 32px; margin-top: 20px;"></a>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
              <a href="/history" class="dropdown-item">Order History</a>
              <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </ul>
      </div>
      <!-- <li class="nav-item cta"><a href="Login.html" class="btn btn-primary nav-link">Login</a></li> -->
      @endauth
    </ul>

  </div>
</div>