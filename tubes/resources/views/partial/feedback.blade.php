<div class="container">
    <div class="row justify-content-center pb-4">
      <div class="col-md-7 text-center heading-section ftco-animate">
        <h2 class="mb-4">Tourist Feedback</h2>
      </div>
    </div>
    <div class="row ftco-animate">
      <div class="col-md-12">
        <div class="carousel-testimony owl-carousel ftco-owl">
          <div class="item">
            <div class="testimony-wrap py-4">
              <div class="text">
                <p class="mb-4">Aplikasi ini sangat membantu sekali dalam mengatur perjalanan yang last minute karena sudah tersedia semua fiturnya</p>
                <div class="d-flex align-items-center">
                    <div class="user-img" style="background-image: url('{{ asset('template/images/Ellipse\ \(1\).png')}}')"></div>
                    <div class="pl-3">
                        <p class="name">Roger Scott</p>
                        <span class="position">IT Manager</span>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="testimony-wrap py-4">
              <div class="text">
                <p class="mb-4">sangat mudah dalam membuat rencana perjalanan bagi orang yang belum pernah kelombok karena tersedia paket- paket perjalanan yang lengkap banget itinerary nya</p>
                <div class="d-flex align-items-center">
                    <div class="user-img" style="background-image: url('{{ asset('template/images/Ellipse\ \(2\).png')}}')"></div>
                    <div class="pl-3">
                        <p class="name">Naomi Scott</p>
                        <span class="position">Celebgram</span>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="testimony-wrap py-4">
              <div class="text">
                <p class="mb-4">rencana perjalananya asik banget tour guide nya ramah dan itinerary nya lengkap bgt</p>
                <div class="d-flex align-items-center">
                    <div class="user-img" style="background-image: url('{{ asset('template/images/Ellipse\ \(3\).png')}}')"></div>
                    <div class="pl-3">
                        <p class="name">Elizabeth Shawn</p>
                        <span class="position">Dentist</span>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="testimony-wrap py-4">
              <div class="text">
                <p class="mb-4">tempat-tempat nya seru banget mulai ke gili trawangan, pantai pink beach, pemilihan hotel, tour guide nya juga ramah banget pokoknya the best banget aplikasi ini</p>
                <div class="d-flex align-items-center">
                    <div class="user-img" style="background-image: url('{{ asset('template/images/Ellipse\ \(4\).png')}}')"></div>
                    <div class="pl-3">
                        <p class="name">Emma Watson</p>
                        <span class="position">Artist</span>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="testimony-wrap py-4">
              <div class="text">
                <p class="mb-4">aplikasi nya cocok banget buat anak last minute kayak aku yang serba dadakan terus ada paket-paket yang lengkap jdi makin mudah buat explore lombok plus tour guide nya ramah banget</p>
                <div class="d-flex align-items-center">
                    <div class="user-img" style="background-image: url('{{ asset('template/images/Ellipse.png')}}')"></div>
                    <div class="pl-3">
                        <p class="name">Donny Robert Jr</p>
                        <span class="position">Ironman</span>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>