<div class="container">
    <div class="row justify-content-center pb-4">
  <div class="col-md-12 heading-section text-center ftco-animate">
    <h2 class="mb-4">Best Place Destination</h2>
  </div>
</div>
<div class="row">
    <div class="col-md-3 ftco-animate">
        <div class="project-destination">
            <a class="img" style="background-image: url('{{ asset('template/images/gilitrawangan1.jpg')}}');">
                <div class="text">
                    <h3>Gilitrawangan</h3>
                    <span>3 Tours Days</span>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-3 ftco-animate">
        <div class="project-destination">
            <a class="img" style="background-image: url('{{ asset('template/images/pinkbeach.jpg')}}');">
                <div class="text">
                    <h3>Pink Beach</h3>
                    <span>2 Tours Days</span>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-3 ftco-animate">
        <div class="project-destination">
            <a class="img" style="background-image: url('{{ asset('template/images/sendanggile2.jpg')}}');">
                <div class="text">
                    <h3>Sendang Gile Waterfall</h3>
                    <span>2 Tours Days</span>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-3 ftco-animate">
        <div class="project-destination">
            <a class="img" style="background-image: url('{{ asset('template/images/Mandalika\ -\ Lombok\ Indonesia.jpg')}}');">
                <div class="text">
                    <h3>Mandalika Circuit International</h3>
                    <span>5 Tours Days</span>
                </div>
            </a>
        </div>
    </div>
</div>
</div>