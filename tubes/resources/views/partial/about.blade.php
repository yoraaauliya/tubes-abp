<div class="container">
    <div class="row d-flex">
      <div class="col-md-6 order-md-last heading-section pl-md-5 ftco-animate">
          <h2 class="mb-4">Liburan bersama BOOKTRIP</h2>
        <p>Pulau Lombok di Provinsi Nusa Tenggara Barat menyimpan banyak keunikan. 
            Selain menjadi destinasi wisata bahari dengan Tiga Gili yang menawan hati, 
            daerah ini juga menjadi rumah bagi berbagai adat dan budaya yang menarik untuk diketahui. 
            Bukan hal yang aneh jika kemudian Lombok menjadi pesaing kuat Pulau Dewata dalam menggaet wisatawan dunia. 
            Terbaru adalah Sirkuit Mandalika yang menjadi destinasi olahraga terbaru yang sedang ramai dibicarakan. 
            Dari banyaknya destinasi tentunya membuat wisatawan bingung memilih destinasi yang ingin mereka kunjungi. 
            Aplikasi BOOKTRIP (Lombok Trip) dibangun untuk membantu wisatawan mengunjungi destinasi di Lombok.</p>
      </div>
      <div class="col-md-6">
          <div class="row">
              <div class="col-md-6 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block">
                  <div class="icon"><span class="flaticon-paragliding"></span></div>
                  <div class="media-body">
                    <h3 class="heading mb-3">Activities</h3>
                    <p>Menjelajahi destinasi wisata di Pulau Lombok yang memiliki daya tarik bagi wisatawan</p>
                  </div>
                </div>      
              </div>
              <div class="col-md-6 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block">
                  <div class="icon"><span class="flaticon-route"></span></div>
                  <div class="media-body">
                    <h3 class="heading mb-3">Travel Arrangements</h3>
                    <p>Agenda perjalanan yang terencana mengunjungi destinasi di Lombok</p>
                  </div>
                </div>    
              </div>
              <div class="col-md-6 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block">
                  <div class="icon"><span class="flaticon-tour-guide"></span></div>
                  <div class="media-body">
                    <h3 class="heading mb-3">Private Guide</h3>
                    <p>Perjalanan anda didampingi oleh tour guide yang siap menemani perjalanan wisata anda di Lombok</p>
                  </div>
                </div>      
              </div>
              <div class="col-md-6 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block">
                  <div class="icon"><span class="flaticon-map"></span></div>
                  <div class="media-body">
                    <h3 class="heading mb-3">Location Manager</h3>
                    <p>Menawarkan berbagai macam lokasi wisata di Lombok yang menarik.</p>
                  </div>
                </div>      
              </div>
          </div>
      </div>
    </div>
  </div>