<!DOCTYPE html>
<html lang="en">
	<head>
		<title>BookTrip</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		@include('partial.head')
		
	</head>
  <body>
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    @include('partial.navbar')
        
	  </nav>
    <!-- END nav -->
    
    <section class="ftco-section services-section bg-light">
		@include('partial.about')
    </section>

    <section class="ftco-section testimony-section bg-bottom" style="background-image: url('{{ asset('template/images/bg_3.jpg')}}');">
      @include('partial.feedback')
    </section>

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  @include('partial.script')
  </body>
</html>