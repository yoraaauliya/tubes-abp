<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Destinasi;
use Illuminate\Support\Facades\DB;

class DestinasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $destinasi = Destinasi::all();
        return view('destinasi.index', compact('destinasi'));
    }

    public function indexAdmin()
    {
        $destinasi = Destinasi::all();
        return view('destinasi.Manage.indexAdmin', compact('destinasi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('destinasi.Manage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'foto_1' => 'required|image|mimes:jpeg,png,jpg,svg,webp|max:2048',
            'foto_2' => 'required|image|mimes:jpeg,png,jpg,svg,webp|max:2048',
            'foto_hotel' => 'required|image|mimes:jpeg,png,jpg,svg,webp|max:2048',
        ]);

        $imageName1 = time().'.'.$request->foto_1->extension();
        $request->foto_1->move(public_path('gambar/img_1'), $imageName1);

        $imageName2 = time().'.'.$request->foto_2->extension();
        $request->foto_2->move(public_path('gambar/img_2'), $imageName2);

        $imageName3 = time().'.'.$request->foto_hotel->extension();
        $request->foto_hotel->move(public_path('gambar/img_3'), $imageName3);


        $destinasi = new Destinasi;
        $destinasi->destinasi = $request->destinasi;
        $destinasi->keberangkatan = $request->keberangkatan;
        $destinasi->harga = $request->harga;
        $destinasi->durasi_tour = $request->durasi_tour;
        $destinasi->durasi_tour_guide = $request->durasi_tour_guide;
        $destinasi->lokasi_hotel = $request->lokasi_hotel;
        $destinasi->bed = $request->bed;
        $destinasi->bathroom = $request->bathroom;
        $destinasi->foto_1 = $imageName1;
        $destinasi->foto_2 = $imageName2;
        $destinasi->foto_hotel = $imageName3;
        $destinasi->desc = $request->desc;
        $destinasi->save();

        
        return redirect('/crudDestination');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $destinasi = Destinasi::find($id);

        return view('destinasi.show', compact('destinasi'));
    }


    public function showdestination($id)
    {
        $destinasi = Destinasi::find($id);

        return view('order.index', compact('destinasi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $destinasi = DB::table('destinasi')->where('id', $id)->first();
        return view('destinasi.Manage.edit', compact('destinasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $destinasi = Destinasi::findOrFail($id);

        if($request->has('foto_1')){
            $path = "gambar/img_1";
            File::delete($path . $destinasi->foto_1);
            $imageName = time().'.'.$request->foto_1->extension();
            $request->foto_1->move(public_path('gambar/img_1'), $imageName);
            $destinasi->foto_1 = $imageName;
        }
        if($request->has('foto_2')){
            $path = "gambar/img_2";
            File::delete($path . $destinasi->foto_2);
            $imageName2 = time().'.'.$request->foto_2->extension();
            $request->foto_2->move(public_path('gambar/img_2'), $imageName2);
            $destinasi->foto_2 = $imageName2;
        }
        if($request->has('foto_hotel')){
            $path = "gambar/img_3";
            File::delete($path . $destinasi->foto_hotel);
            $imageName3 = time().'.'.$request->foto_hotel->extension();
            $request->foto_hotel->move(public_path('gambar/img_3'), $imageName3);
            $destinasi->foto_hotel = $imageName3;
        }
        $destinasi->destinasi = $request->destinasi;
        $destinasi->keberangkatan = $request->keberangkatan;
        $destinasi->harga = $request->harga;
        $destinasi->durasi_tour = $request->durasi_tour;
        $destinasi->durasi_tour_guide = $request->durasi_tour_guide;
        $destinasi->lokasi_hotel = $request->lokasi_hotel;
        $destinasi->bed = $request->bed;
        $destinasi->bathroom = $request->bathroom;
        $destinasi->desc = $request->desc;
        $destinasi->save();

        return redirect('/crudDestination');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Destinasi::find($id);
        $model->delete();
        return redirect('/crudDestination');
    }
}
