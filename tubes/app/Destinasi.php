<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destinasi extends Model
{
    protected $table = 'destinasi';
    protected $fillabel = ['destinasi','keberangkatan', 'durasi_tour', 'durasi_tour_guide', 'lokasi_hotel', 'bed', 'bathroom', 'foto_1', 'foto_2', 'foto_hotel', 'harga', 'desc'];
}
