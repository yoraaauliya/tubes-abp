<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $fillabel = ['email','bank', 'noHp', 'user_id', 'dest_id'];
}
