<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDestinasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destinasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('destinasi');
            $table->string('keberangkatan');
            $table->string('durasi_tour');
            $table->string('durasi_tour_guide');
            $table->string('lokasi_hotel');
            $table->integer('bed');
            $table->integer('bathroom');
            $table->string('foto_1');
            $table->string('foto_2');
            $table->string('foto_hotel');
            $table->string('harga');
            $table->text('desc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destinasi');
    }
}
